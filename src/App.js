import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";
import {
  AttendanceProvider,
  AuthProvider,
  EventProvider,
  EnrolmentProvider,
  FacePhotoProvider,
  FaceThresholdDistanceProvider,
  NavbarProvider,
  NotificationProvider,
} from "./context";
import {
  EventDetails,
  Dashboard,
  MainMenu,
  NoFound,
  Notifications,
  Profile,
  SignIn,
  SignUp,
  InEventAttendanceHistory,
  SingleAttendanceHistory,
  AttendanceRoom,
  UndefinedCardIDAndRole,
  PrivacyPolicy,
  TermCondition,
  UserGuidelines,
  Identify,
} from "./pages/common";
import {
  AttendanceForm,
} from "./pages/servantPage";
import { FaceGallery } from "./pages/memberPage";
import {
  AuthRoute,
  ServantRoute,
  ProtectedRoute,
  MemberRoute,
  UndefinedCardIDAndRoleRoute,
} from "./routes";
import Testing from "./Testing";
import "lazysizes";

function App() {
  return (
    <NavbarProvider>
      <AuthProvider>
        <NotificationProvider>
          <AttendanceProvider>
            <EventProvider>
              <EnrolmentProvider>
                <FacePhotoProvider>
                  <FaceThresholdDistanceProvider>
                    <Router>
                      <Switch>
                        <ProtectedRoute exact path="/" component={MainMenu} />
                        <ProtectedRoute
                          exact
                          path="/signin"
                          component={SignIn}
                        />
                        <ProtectedRoute
                          exact
                          path="/signup"
                          component={SignUp}
                        />

                        <UndefinedCardIDAndRoleRoute
                          exact
                          path="/aboutYourself"
                          component={UndefinedCardIDAndRole}
                        />
                        <AuthRoute
                          exact
                          path="/dashboard"
                          component={Dashboard}
                        />
                        <AuthRoute exact path="/profile" component={Profile} />
                        <AuthRoute
                          exact
                          path="/notification"
                          component={Notifications}
                        />
                        <AuthRoute
                          exact
                          path="/event/:id"
                          component={EventDetails}
                        />
                        <AuthRoute exact path='/testing' component={Testing} />
                        <ServantRoute
                          exact
                          path="/event/:id/attendanceForm"
                          component={AttendanceForm}
                        />
                        <AuthRoute
                          exact
                          path="/event/:eventID/attendanceRoom/:attendanceID"
                          component={AttendanceRoom}
                        />

                        <AuthRoute
                          exact
                          path="/event/:id/attendanceList"
                          component={InEventAttendanceHistory}
                        />

                        <AuthRoute
                          exact
                          path="/event/:eventID/attendanceList/:attendanceID"
                          component={SingleAttendanceHistory}
                        />

                        <MemberRoute
                          exact
                          path="/facegallery"
                          component={FaceGallery}
                        />
                        <Route
                          exact
                          path="/termandcondition"
                          component={TermCondition}
                        />
                        <Route
                          exact
                          path="/privacypolicy"
                          component={PrivacyPolicy}
                        />
                        <Route
                          exact
                          path="/userguidelines"
                          component={UserGuidelines}
                        />
                        <Route
                          exact
                          path="/identity"
                          component={Identify}
                        />
                        <AuthRoute
                          exact
                          path="/identify/:eventID/attendanceRoom/:attendanceID"
                          component={Identify}
                        />
                        <AuthRoute component={NoFound} />
                      </Switch>
                    </Router>
                  </FaceThresholdDistanceProvider>
                </FacePhotoProvider>
              </EnrolmentProvider>
            </EventProvider>
          </AttendanceProvider>
        </NotificationProvider>
      </AuthProvider>
    </NavbarProvider>
  );
}

export default App;
