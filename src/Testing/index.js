import React, { useState } from 'react';
import { Button, Input, message } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import { useQuery, useMutation } from '@apollo/react-hooks';
import gql from 'graphql-tag';
export default () => {
  const [eventID, SetEventID] = useState('');
  const [createNotificationCallback, createNotificationStatus] = useMutation(
    CREATE_NOTIFICATION_MUTATION,
    {
      onError: (err) => {
        message.error(err.message);
      },
    }
  );
  const [
    deleteAllNotificationCallback,
    deleteAllNotificationStatus,
  ] = useMutation(DELETE_ALL_NOTIFICATION_MUTATION, {
    onError: (err) => {
      message.error(err.message);
    },
  });
  const [createEventCallback, createEventStatus] = useMutation(
    CREATE_COURSE_MUTATION,
    {
      onError: (err) => {
        message.error(err.message);
      },
    }
  );
  const [deleteAllEventCallback, deleteAllEventStatus] = useMutation(
    DELETE_ALL_COURSE_MUTATION,
    {
      onError: (err) => {
        message.error(err.message);
      },
    }
  );
  const [registerMemberCallback, registerMemberStatus] = useMutation(
    REGISTER_STUDENT_MUTATION,
    {
      onError: (err) => {
        message.error(err.message);
      },
      variables: {
        eventID: eventID,
      },
    }
  );
  return (
    <div>
      <h1>Testing</h1>
      <Button
        onClick={() => createNotificationCallback()}
        disabled={createNotificationStatus.loading}
      >
        Create 50 notification{' '}
        {createNotificationStatus.loading && <LoadingOutlined />}
      </Button>
      <Button
        onClick={() => deleteAllNotificationCallback()}
        disabled={deleteAllNotificationStatus.loading}
      >
        Delete All notification{' '}
        {deleteAllNotificationStatus.loading && <LoadingOutlined />}
      </Button>
      <Button
        onClick={() => createEventCallback()}
        disabled={createEventStatus.loading}
      >
        Create 50 events {createEventStatus.loading && <LoadingOutlined />}
      </Button>
      <Button
        onClick={() => deleteAllEventCallback()}
        disabled={deleteAllEventStatus.loading}
      >
        Delete All events{' '}
        {deleteAllEventStatus.loading && <LoadingOutlined />}
      </Button>
      <Button
        onClick={() => registerMemberCallback()}
        disabled={registerMemberStatus.loading}
      >
        Register 10 member and enrol to event{' '}
        {registerMemberStatus.loading && <LoadingOutlined />}
      </Button>
      <input
        type='text'
        name='eventID'
        onChange={(e) => SetEventID(e.target.value)}
      ></input>
    </div>
  );
};

const CREATE_NOTIFICATION_MUTATION = gql`
  mutation createNotification {
    createNotification
  }
`;

const DELETE_ALL_NOTIFICATION_MUTATION = gql`
  mutation deleteAllNotification {
    deleteAllNotification
  }
`;

const CREATE_COURSE_MUTATION = gql`
  mutation testingCreateEvent {
    testingCreateEvent
  }
`;

const DELETE_ALL_COURSE_MUTATION = gql`
  mutation testingDeleteAllEvent {
    testingDeleteAllEvent
  }
`;

const REGISTER_STUDENT_MUTATION = gql`
  mutation testingRegisterMember($eventID: String!) {
    testingRegisterMember(eventID: $eventID)
  }
`;
