import { useMutation } from "@apollo/react-hooks";
import { Button, Card, Form, Input, Layout, message } from "antd";
import React, { useContext } from "react";
import { Footer, Greeting, Navbar, PageTitleBreadcrumb } from "../sharedLayout";
import { AuthContext, EventContext } from "../../../context";
import { CheckError } from "../../../utils/ErrorHandling";
import { ADD_COURSE_MUTATION } from "../../../graphql/mutation";
import { useForm } from "../../../utils/hooks";

const { Content } = Layout;

export default ({ refetchTableTotal, refetchTable }) => {
  const { user } = useContext(AuthContext);
  const { addEvent } = useContext(EventContext);
  const { onSubmit, onChange, values } = useForm(submitCallback);

  const [addEventCallback, { loading }] = useMutation(ADD_COURSE_MUTATION, {
    update() {
      message.success("Create event successfully.");
      refetchTableTotal();
      refetchTable();
    },
    onError(err) {
      CheckError(err);
    },
    variables: {
      name: values.eventName,
      code: values.eventCode,
      session: values.eventSession,
    },
  });

  function submitCallback() {
    addEventCallback();
  }

  return (
    <Content>
      <Card title="New Event Form" className="addEvent__card">
        <br />
        <Form className="addEvent__form" name="basic" onFinish={onSubmit}>
          <Form.Item
            label="Event Location"
            name="eventCode"
            rules={[{ required: true, message: "Please input event location!" }]}
          >
            <Input
              name="eventCode"
              placeholder="Enter event code" 
              onChange={onChange}
            />
          </Form.Item>

          <Form.Item
            label="Event Name"
            name="eventName"
            rules={[{ required: true, message: "Please input event name!" }]}
          >
            <Input
              name="eventName"
              placeholder="Enter event name"
              onChange={onChange}
            />
          </Form.Item>

          <Form.Item
            label="Event Session"
            name="eventSession"
            rules={[
              { required: true, message: "Please input event session!" },
            ]}
          >
            <Input
              name="eventSession"
              placeholder="Enter event session"
              onChange={onChange}
            />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit" loading={loading}>
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </Content>
  );
};
