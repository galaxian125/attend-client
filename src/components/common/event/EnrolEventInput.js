import { Layout, Form, Card, Button, Input, message } from 'antd';
import { useMutation } from '@apollo/react-hooks';
import { ENROL_COURSE_MUTATION } from '../../../graphql/mutation';
import React, { useState } from 'react';

import { CheckError } from "../../../utils/ErrorHandling";

export default () => {
  const [eventID, setEventID] = useState('');
  const [enrolEventCallback, enrolEventStatus] = useMutation(
    ENROL_COURSE_MUTATION,
    {
      onCompleted(data) {
        message.success(
          'Enrol success'
        );
      },
      onError(err) {
        CheckError(err);
      },
      variables: { id: eventID },
    }
  );
  return (
    <div>
      <p className='alert'>🢃 Enter Event ID for new enrolment</p>

      <Form style={{ display: 'flex' }} onFinish={() => enrolEventCallback()}>
        <Form.Item
          label='Event ID'
          name='eventID'
          rules={[{ required: true, message: 'Please input event ID!' }]}
        >
          <Input
            name='eventCode'
            placeholder='Enter event Code to enroll'
            onChange={(e) => setEventID(e.target.value)}
          />
        </Form.Item>

        <Form.Item>
          <Button
            type='primary'
            loading={enrolEventStatus.loading}
            style={{ marginLeft: '10px' }}
            htmlType='submit'
          >
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};
