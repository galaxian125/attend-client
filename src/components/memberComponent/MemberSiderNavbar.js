import { Avatar } from 'antd';
import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { NavbarContext } from '../../context';
import { MemberMenu } from './';
import './MemberSiderNavbar.css';
import { APP_LOGO_URL } from '../../assets';

export default () => {
  const { collapsed } = useContext(NavbarContext);
  return (
    <div className='memberSiderNavbar'>
      <Link to='/dashboard'>
        <div className='memberSiderNavbar__logo'>
          <Avatar
            shape="square"
            className='avatar'
            size='large'
            alt='Face In'
            title='Face in (Member Version)'
            src={APP_LOGO_URL.link}
            onError={(err) => {
              console.log(err);
            }}
          />
          <div
            className={
              !collapsed
                ? 'memberSiderNavbar__text'
                : 'memberSiderNavbar__text__hidden'
            }
          >
            <span className='memberSiderNavbar__text__item'>User</span>
            <span className='memberSiderNavbar__text__item'>Panel</span>
          </div>
        </div>
      </Link>

      <div className='memberSiderNavbar__menu'>
        <MemberMenu />
      </div>
    </div>
  );
};
