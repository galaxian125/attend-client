import { Avatar } from 'antd';
import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { NavbarContext } from '../../context';
import { ServantMenu } from './';
import './ServantSiderNavbar.css';
import { APP_LOGO_URL } from '../../assets';

export default () => {
  const { collapsed } = useContext(NavbarContext);

  return (
    <div className='servantSiderNavbar'>
      <Link to='/dashboard'>
        <div className='servantSiderNavbar__logo'>
          <Avatar
            shape="square"
            className='avatar'
            size='large'
            alt='Face In'
            src={APP_LOGO_URL.link}
            onError={(err) => {
              console.log(err);
            }}
          />
          <div
            className={
              !collapsed
                ? 'servantSiderNavbar__text'
                : 'servantSiderNavbar__text__hidden'
            }
          >
            <span className='servantSiderNavbar__text__item'>Admin</span>
            <span className='servantSiderNavbar__text__item'>Panel</span>
          </div>
        </div>
      </Link>

      <div className='servantSiderNavbar__menu'>
        <ServantMenu />
      </div>
    </div>
  );
};
