import React, { createContext, useReducer } from 'react';
import { actionTypes } from '../globalData';

const initialState = {
  events: [],
  newEvents: [],
  fetchedDone: false,
  initialAccess: true
};

const EventContext = createContext({});
 //
function eventReducer(state, action) {
  switch (action.type) {
    case actionTypes.FETCH_DONE_ACTION:
      return {
        ...state,
        fetchedDone: action.done,
      };

    case actionTypes.LOAD_COURSES_ACTION:
      return {
        ...state,
        events: [...state.newEvents, ...action.events],
        initialAccess: false
      };

    case actionTypes.ADD_COURSE_ACTION:
      //here we check whether the events is not fully loaded, mean the event should be included in the fetch
      if (state.initialAccess) {
        return {
          ...state,
        };
      }
      return {
        ...state,
        newEvents: [action.event, ...state.newEvents],
      };
  }
}

function EventProvider(props) {
  const [state, dispatch] = useReducer(eventReducer, initialState);

  function loadEvents(events) {
    dispatch({ type: actionTypes.LOAD_COURSES_ACTION, events });
  }

  function setFetchedDone(done) {
    dispatch({ type: actionTypes.FETCH_DONE_ACTION, done });
  }

  function addEvent(event) {
    dispatch({
      type: actionTypes.ADD_COURSE_ACTION,
      event,
    });
  }

  return (
    <EventContext.Provider
      value={{
        events: state.events,
        fetchedDone: state.fetchedDone,
        setFetchedDone,
        loadEvents,
        addEvent,
      }}
      {...props}
    />
  );
}

export { EventContext, EventProvider };
