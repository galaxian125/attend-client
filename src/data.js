export const NavbarData = {
  Main: {
    children: [
      {
        title: "Sign In",
        href: "signin",
      },
      {
        title: "Sign Up",
        href: "signup",
      },
    ],
  },
  Member: {
    children: [
      {
        title: "Home",
        href: "dashboard",
      },
      ,
      {
        title: "Enrol Event",
        href: "enrolevent",
      },
      {
        title: "Face Photo",
        href: "facephoto",
      },
      {
        title: "View Attendance",
        href: "viewattendance",
      },
    ],
  },
  Servant: {
    children: [
      {
        title: "Home",
        href: "dashboard",
      },
      {
        title: "Add Event",
        href: "addevent",
      },
      {
        title: "Take Attendance",
        href: "takeattendance",
      },
      {
        title: "View Attendance",
        href: "viewattendance",
      },
      {
        title: "Attendance History",
        href: "attendancehistory",
      },
    ],
  },
};
