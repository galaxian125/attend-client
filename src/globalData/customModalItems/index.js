export const modalItems = {
  event: {
    name: 'event',
    action: { delete: 'Delete', withdraw: 'Withdraw' },
  },
  participant: {
    name: 'member',
    action: { kick: 'Kick' },
  },
  enrolment: {
    name: 'enrolment',
    action: { approve: 'Approve', reject: 'Reject' },
  },
  facePhoto: { name: 'photo', action: { delete: 'Delete' } },
  attendance: { name: 'attendance', action: { delete: 'Delete' } },
};
