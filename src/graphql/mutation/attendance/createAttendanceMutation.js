import gql from 'graphql-tag';

export const CREATE_ATTENDANCE_MUTATION = gql`
  mutation createAttendance(
    $eventID: String!
    $date: String!
    $time: String!
  ) {
    createAttendance(
      attendanceInput: {
        eventID: $eventID
        date: $date
        time: $time
      }
    ) {
      _id
      event {
        _id
        shortID
        name
        code
        session
      }
      date
      time
      mode
    }
  }
`;
