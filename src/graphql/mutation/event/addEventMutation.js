import gql from 'graphql-tag';

export const ADD_COURSE_MUTATION = gql`
  mutation createEvent($code: String!, $name: String!, $session: String!) {
    createEvent(eventInput: { code: $code, name: $name, session: $session }) {
      _id
      shortID
      code
      name
      session
      createdAt
    }
  }
`;
