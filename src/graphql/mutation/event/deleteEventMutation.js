import gql from 'graphql-tag';

export const DELETE_COURSE_MUTATION = gql`
  mutation deleteEvent($id: ID!) {
    deleteEvent(eventID: $id) {
      _id
      name
      code
      session
    }
  }
`;
