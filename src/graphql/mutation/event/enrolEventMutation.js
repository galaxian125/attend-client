import gql from 'graphql-tag';

export const ENROL_COURSE_MUTATION = gql`
  mutation enrolEvent($id: ID!) {
    enrolEvent(eventID: $id)
  }
`;
