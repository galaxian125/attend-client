import gql from 'graphql-tag';

export const WITHDRAW_COURSE_MUTATION = gql`
  mutation withdrawEvent($id: ID!) {
    withdrawEvent(eventID: $id)
  }
`;
