import gql from 'graphql-tag';

export const KICK_PARTICIPANT_MUTATION = gql`
  mutation kickParticipant($participantID: ID!, $eventID: String!) {
    kickParticipant(participantID: $participantID, eventID: $eventID)
  }
`;

export const WARN_PARTICIPANT_MUTATION = gql`
  mutation warnParticipant($participantID: ID!, $eventID: String!) {
    warnParticipant(participantID: $participantID, eventID: $eventID)
  }
`;

export const ADD_PARTICIPANT_MUTATION = gql`
  mutation addParticipant($email: String!, $eventID: String!) {
    addParticipant(email: $email, eventID: $eventID) {
      _id
      firstName
      lastName
      cardID
      profilePictureURL
    }
  }
`;
