import gql from "graphql-tag";

export const CREATE_TRX_MUTATION = gql`
  mutation createTrx($attendanceID: ID!, $memberID: ID!) {
    createTrx(
      trxInput: { attendanceID: $attendanceID, memberID: $memberID }
    )
  }
`;
