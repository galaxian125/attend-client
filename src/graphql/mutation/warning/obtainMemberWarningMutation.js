import gql from 'graphql-tag';

export const OBTAIN_STUDENT_WARNING_MUTATION = gql`
  mutation obtainMemberWarning($participantID: ID!, $eventID: String!) {
    obtainMemberWarning(participantID: $participantID, eventID: $eventID)
  }
`;
