import gql from "graphql-tag";

export const FETCH_ATTENDANCE_LIST_COUNT_IN_COURSE_QUERY = gql`
  query getAttendanceListCountInEvent($eventID: String!) {
    getAttendanceListCountInEvent(eventID: $eventID)
  }
`;

export const FETCH_ATTENDANCE_QUERY = gql`
  query getAttendance($attendanceID: ID!) {
    getAttendance(attendanceID: $attendanceID) {
      _id
      event {
        _id
        name
        code
        session
        shortID
      }
      time
      date
      mode
      isOn
    }
  }
`;

export const FETCH_ATTENDANCE_LIST_IN_COURSE_QUERY = gql`
  query getAttendanceListInEvent(
    $eventID: String!
    $currPage: Int!
    $pageSize: Int!
  ) {
    getAttendanceListInEvent(
      eventID: $eventID
      currPage: $currPage
      pageSize: $pageSize
    ) {
      event{
        _id
        shortID
        code
        name
        session
      }
      attendanceList{
        _id
        time
        date
        mode
        isOn
      }
    }
  }
`;
