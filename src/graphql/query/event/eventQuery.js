import gql from 'graphql-tag';


export const FETCH_COURSES_COUNT_QUERY = gql`
  query getEventsCount {
    getEventsCount
  }
`;

export const FETCH_COURSE_QUERY = gql`
  query getEvent($id: ID!) {
    getEvent(eventID: $id) {
        _id
        shortID
        name
        code
        session
    }
  }
`;

export const FETCH_PARTICIPANTS_QUERY = gql`
  query getParticipants($id: ID!) {
    getParticipants(eventID: $id) {
      _id
      firstName
      lastName
      profilePictureURL
      cardID
    }
  }
`;

export const FETCH_COURSES_QUERY = gql`
  query getEvents($currPage: Int!, $pageSize: Int!) {
    getEvents(currPage: $currPage, pageSize: $pageSize) {
      events {
        _id
        shortID
        creator {
          firstName
          lastName
          cardID
        }
        name
        code
        session
        createdAt
      }
    }
  }
`;