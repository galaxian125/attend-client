import gql from 'graphql-tag';

export const FETCH_FACE_PHOTOS_COUNT_QUERY = gql`
  query getFacePhotosCount {
    getFacePhotosCount
  }
`;

export const FETCH_FACE_PHOTOS_QUERY = gql`
  query getFacePhotos($cursor: ID, $limit: Int!) {
    getFacePhotos(cursor: $cursor, limit: $limit) {
      facePhotos {
        _id
        faceDescriptor
        photoURL
        createdAt
      }
      hasNextPage
    }
  }
`;

export const FETCH_FACE_MATCHER_IN_COURSE_QUERY = gql`
  query getFaceMatcherInEvent($eventID: String!) {
    getFaceMatcherInEvent(eventID: $eventID) {
      event {
        _id
        code
        name
        session
        shortID
      }
      matcher {
        member {
          _id
          firstName
          lastName
          cardID
          profilePictureURL
        }
        facePhotos {
          faceDescriptor
          photoURL
        }
      }
    }
  }
`;
