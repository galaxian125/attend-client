import gql from 'graphql-tag';

export const GET_WARNING_COUNT_QUERY = gql`
  query getWarningCount($eventID: ID!) {
    getWarningCount(eventID: $eventID)
  }
`;
