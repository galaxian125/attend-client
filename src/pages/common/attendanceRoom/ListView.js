import { UserOutlined } from "@ant-design/icons";
import { Avatar, Button, List } from "antd";
import React, { useEffect, useState } from "react";
import moment from "moment";

export default ({ memberList }) => {
  const [isPhotoVisible, setIsPhotoVisible] = useState({});

  /*const handleDoubleClick = (item) => {
    if (memberList.some(absentee=>absentee.member._id!=item.member._id)){
    setmemberList((prevState) =>
      prevState.filter((absentee) => absentee !== item)
    );
    }
    else
    setmemberList(prevState=>[item, ...prevState])

    console.log("target", item)
    console.log("memberList",memberList)
  };*/

  useEffect(() => {
    memberList.map((memberList) => {
      setIsPhotoVisible({
        ...isPhotoVisible,
        [memberList.member._id]: false,
      });
    });
  }, []);

  const handleShowPhoto = (id) => {
    setIsPhotoVisible({ ...isPhotoVisible, [id]: !isPhotoVisible[id] });
  };

  return (
    <List
      pagination={{
        pageSize: 10,
      }}
      itemLayout="horizontal"
      dataSource={memberList}
      renderItem={(item) => (
        <List.Item
          key={item.member._id}
          style={{ display: "flex", justifyContent: "center" }}
        >
          <List.Item.Meta
            avatar={
              <Avatar
                src={item.member.profilePictureURL}
                icon={<UserOutlined />}
              />
            }
            title={
              <span style={{ cursor: "pointer" }}>
                {item.member.firstName} {item.member.lastName} (
                {item.member.cardID}){"  "}
              </span>
            }
            description={
              <div style={{ display: "flex", flexDirection: "column" }}>
                <div>
                  <p>Check in: {item.attend_at? moment(item.attend_at).format("DD/MM/YYYY h:mm:ss a"): "-"}</p>
                </div>
                <div>
                  Number of sample photo:{" "}
                  {item.facePhotos?.length === 0 ? (
                    <span
                      style={{
                        color: "red",
                        fontWeight: 900,
                        fontSize: "20px",
                      }}
                    >
                      0
                    </span>
                  ) : (
                    item.facePhotos?.length || (
                      <span
                        style={{
                          color: "red",
                          fontWeight: 900,
                          fontSize: "20px",
                        }}
                      >
                        0
                      </span>
                    )
                  )}
                </div>

                {item.facePhotos?.length > 0 && (
                  <>
                    <div>
                      <Button onClick={() => handleShowPhoto(item.member._id)}>
                        {!isPhotoVisible[item.member._id]
                          ? "Show Photo"
                          : "Hide Photo"}
                      </Button>
                    </div>
                    {isPhotoVisible[item.member._id] && (
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "flex-start",
                          alignItems: "space-between",
                          flexWrap: "wrap",
                        }}
                      >
                        {item.facePhotos?.map((photo) => (
                          <div key={photo._id}>
                            <img
                              style={{
                                width: "100px",
                                height: "100px",
                                margin: "10px",
                              }}
                              data-src={photo.photoURL}
                              className="lazyload"
                              src={`${process.env.PUBLIC_URL}/img/loader.gif`}
                              alt={item.firstName}
                            />
                          </div>
                        ))}
                      </div>
                    )}
                  </>
                )}
              </div>
            }
          />
        </List.Item>
      )}
    />
  );
};
