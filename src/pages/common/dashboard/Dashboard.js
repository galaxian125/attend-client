import {
  ArrowRightOutlined,
  EditFilled,
  DeleteFilled,
  RedoOutlined,
} from "@ant-design/icons";
import { useMutation, useQuery } from "@apollo/react-hooks";
import { Card, Button, Layout, message, Skeleton, Space, Table } from "antd";
import React, { useContext, useEffect, useState } from "react";
import AddEventForm from "../../../components/common/event/AddEventForm";
import EnrolEventInput from "../../../components/common/event/EnrolEventInput";
import Modal from "../../../components/common/customModal";
import {
  Footer,
  Greeting,
  Navbar,
  PageTitleBreadcrumb,
} from "../../../components/common/sharedLayout";
import { AuthContext, EventContext } from "../../../context";
import { CheckError } from "../../../utils/ErrorHandling";
import { FETCH_COURSE_LIMIT, modalItems } from "../../../globalData";
import {
  WITHDRAW_COURSE_MUTATION,
  DELETE_COURSE_MUTATION,
} from "../../../graphql/mutation";
import {
  FETCH_COURSES_COUNT_QUERY,
  FETCH_COURSES_QUERY,
} from "../../../graphql/query";
import "./Dashboard.css";

const { Content } = Layout;

export default (props) => {
  const { user } = useContext(AuthContext);
  const titleList = [{ name: "Home", link: "/dashboard" }];

  const columns = [
    {
      title: <strong>No</strong>,
      dataIndex: "bil",
      key: "bil",
      align: "center",
      render: (text) => (
        <Skeleton active loading={loading}>
          {text}
        </Skeleton>
      ),
      sorter: {
        compare: (a, b) => a.bil - b.bil,
        multiple: 2,
      },
    },
    {
      title: <strong>Enroll Code</strong>,
      dataIndex: "shortID",
      key: "shortID",
      align: "center",
      render: (text) => (
        <Skeleton active loading={loading}>
          {text}
        </Skeleton>
      ),
      sorter: (a, b) => a.code.localeCompare(b.date),
    },
    {
      title: <strong>Location</strong>,
      dataIndex: "code",
      key: "code",
      align: "center",

      render: (text) => (
        <Skeleton active loading={loading}>
          {text}
        </Skeleton>
      ),
      sorter: (a, b) => a.code.localeCompare(b.date),
    },
    {
      title: <strong>Name</strong>,
      key: "name",
      dataIndex: "name",
      render: (text) => (
        <Skeleton active loading={loading}>
          {text}
        </Skeleton>
      ),
      align: "center",
      sorter: (a, b) => a.name.localeCompare(b.date),
    },
    {
      title: <strong>Session</strong>,
      key: "session",
      dataIndex: "session",
      render: (text) => (
        <Skeleton active loading={loading}>
          {text}
        </Skeleton>
      ),
      align: "center",
      sorter: (a, b) => a.name.localeCompare(b.date),
    },
    {
      title: <strong>Admin</strong>,
      key: "owner",
      dataIndex: "owner",
      render: (text) => (
        <Skeleton active loading={loading}>
          {text}
        </Skeleton>
      ),
      align: "center",
      sorter: (a, b) => a.name.localeCompare(b.date),
    },
    {
      title: <strong>Action</strong>,
      dataIndex: "action",
      render: (_, record) => (
        <Skeleton active loading={loading}>
          <Button
            onClick={() => handleAccess(record)}
            style={{ margin: "10px" }}
            icon={<ArrowRightOutlined />}
          ></Button>
          <Button
            onClick={() => handleDelete(record)}
            loading={
              selectedEvent.key == record.key && withdrawEventStatus.loading
            }
            disabled={
              selectedEvent.key == record.key && withdrawEventStatus.loading
            }
            style={{ margin: "10px" }}
            type="danger"
            icon={<DeleteFilled />}
          ></Button>
        </Skeleton>
      ),
      align: "center",
    },
  ];

  const parseEventData = (events) => {
    let parsedData = [];
    events.map((c, index) => {
      const tmp = {
        _id: c._id,
        key: c._id,
        bil:
          !loading &&
          tablePagination.pageSize * (tablePagination.current - 1) + index + 1,
        shortID: c.shortID,
        code: c.code,
        name: c.name,
        session: c.session,
        owner: c.creator.firstName+" "+c.creator.lastName+" ("+c.creator.cardID+")"
      };
      parsedData.push(tmp);
    });

    return parsedData;
  };

  const { events, loadEvents } = useContext(EventContext);

  const [tablePagination, setTablePagination] = useState({
    pageSize: FETCH_COURSE_LIMIT,
    current: 1,
    total: 0,
  });

  const [selectedEvent, SetSelectedEvent] = useState({});

  //modal visible boolean
  const [visible, SetVisible] = useState(false);

  //get total events count query
  const totalEventsQuery = useQuery(FETCH_COURSES_COUNT_QUERY, {
    onCompleted(data) {
      // totalAttendancesCount.refetch();
      setTablePagination({
        ...tablePagination,
        total: data.getEventsCount,
      });
    },
    onError(err) {
      CheckError(err);
    },
    notifyOnNetworkStatusChange: true,
  });

  //get list of couse query
  const { data, loading, refetch, fetchMore } = useQuery(FETCH_COURSES_QUERY, {
    onCompleted(data) {
      setTablePagination({
        ...tablePagination,
        total: totalEventsQuery.data?.getEventsCount,
      });

      if (
        totalEventsQuery.data?.getEnrolledEventsCount -
          (tablePagination.current - 1) * tablePagination.pageSize <=
          0 &&
        tablePagination.current !== 1
      ) {
        setTablePagination((prevState) => {
          return {
            ...prevState,
            current: prevState.current - 1,
          };
        });
      }
    },
    onError(err) {
      CheckError(err);
    },
    variables: {
      currPage: tablePagination.current,
      pageSize: tablePagination.pageSize,
    },
    notifyOnNetworkStatusChange: true,
  });

  //withdrawEvent mutation
  const [withdrawEventCallback, withdrawEventStatus] = useMutation(
    WITHDRAW_COURSE_MUTATION,
    {
      onCompleted(data) {
        message.success(data.withdrawEvent);
      },
      update() {
        SetVisible(false);
        refetch();
        totalEventsQuery.refetch();
      },
      onError(err) {
        CheckError(err);
      },
      variables: {
        id: selectedEvent._id,
      },
    }
  );

  const [deleteEventCallback, deleteEventStatus] = useMutation(
    DELETE_COURSE_MUTATION,
    {
      update() {
        SetVisible(false);
        refetch();
        totalEventsQuery.refetch();
      },
      onError(err) {
        CheckError(err);
      },
      variables: {
        id: selectedEvent._id,
      },
    }
  );

  //load events as long as data is fetched
  useEffect(() => {
    if (data) {
      console.log(data);
      loadEvents(data.getEvents.events);
    }
  }, [data]);

  //-> icon is pressed, navigate to event detail page
  const handleAccess = (event) => {
    props.history.push(`/event/${event.shortID}`);
  };

  //delete icon pressed, show modal
  const handleDelete = (event) => {
    SetSelectedEvent(event);
    SetVisible(true);
  };

  //modal open
  const handleOk = (e) => {
    if (user.userLevel == 0) withdrawEventCallback();
    else deleteEventCallback();
  };

  //modal close
  const handleCancel = (e) => {
    SetVisible(false);
  };

  const handleTableChange = (value) => {
    setTablePagination(value);
  };

  return (
    <Layout className="dashboard layout">
      <Navbar />
      <Layout>
        <Greeting />
        <PageTitleBreadcrumb titleList={titleList} />
        <Card>
          {user.userLevel == 0 && <EnrolEventInput />}
          {user.userLevel == 1 && (
            <AddEventForm
              refetchTableTotal={totalEventsQuery.refetch}
              refetchTable={refetch}
            />
          )}

          <Space direction="vertical" className="width100">
            <h1>
              Total {user.userLevel == 0 ? "Enrolled" : "Created"} Event:
              {totalEventsQuery.data?.getEventsCount || 0}
            </h1>
            <Button
              style={{ float: "right" }}
              icon={<RedoOutlined />}
              disabled={loading}
              loading={loading}
              onClick={() => refetch()}
            >
              Refresh Table
            </Button>
            <Table
              scroll={{ x: "max-content" }}
              loading={loading}
              pagination={tablePagination}
              onChange={handleTableChange}
              dataSource={parseEventData(events)}
              columns={columns}
            />
            {console.log(events)}

            {/*modal backdrop*/}
            <Modal
              title={user.userLevel == 0 ? "Withdraw Event" : "Delete Event"}
              action={
                user.userLevel == 0
                  ? modalItems.event.action.withdraw
                  : modalItems.event.action.delete
              }
              itemType={modalItems.event.name}
              visible={visible}
              loading={
                user.userLevel == 0
                  ? withdrawEventStatus.loading
                  : deleteEventStatus.loading
              }
              handleOk={handleOk}
              handleCancel={handleCancel}
              payload={selectedEvent}
            />
          </Space>
        </Card>
        <Footer />
      </Layout>
    </Layout>
  );
};
