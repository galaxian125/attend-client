import { Button, Card, Col, Row } from 'antd';
import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { AuthContext } from '../../../context';

export default ({ event }) => {
  const { user } = useContext(AuthContext);

  return (
    <Row className='eventDetails__row'>
      <Col>
        <Card className='eventDetails__info'>
          <p className='eventDetails__shortID'>ID: {event.shortID}</p>
          <p>
            <strong>Location:</strong> {event.code}
          </p>
          <p>
            <strong>Name:</strong> {event.name}
          </p>
          <p>
            <strong>Session:</strong> {event.session}
          </p>
          {user.userLevel === 1 && (
            <>
              <Button type='primary' className='eventDetails__takeAttendance'>
                <Link to={`/event/${event.shortID}/attendanceForm`}>
                  Create Attendance
                </Link>
              </Button>

              <br />
              <br />
            </>
          )}
          <br />
          <Link to={`/event/${event.shortID}/attendanceList`}>
            Attendance List
          </Link>
        </Card>
      </Col>
    </Row>
  );
};
