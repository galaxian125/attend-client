import { LoadingOutlined } from "@ant-design/icons";
import { useMutation, useQuery } from "@apollo/react-hooks";
import { Card, Form, Layout, message, Select, Switch, Typography } from "antd";
import React, { useContext, useEffect, useState } from "react";
import {
  Footer,
  Greeting,
  Navbar,
  PageTitleBreadcrumb,
} from "../../../components/common/sharedLayout";
import { AuthContext, FaceThresholdDistanceContext } from "../../../context";
import { createMatcher } from "../../../faceUtil";
import { attendanceMode, DEFAULT_ATTENDANCE_MODE } from "../../../globalData";
import {
  EDIT_ATTENDANCE_MODE_MUTATION,
  EDIT_ATTENDANCE_ON_OFF_MUTATION,
} from "../../../graphql/mutation";
import {
  FETCH_ATTENDANCE_QUERY,
  FETCH_FACE_MATCHER_IN_COURSE_QUERY,
} from "../../../graphql/query";
import { CheckError } from "../../../utils/ErrorHandling";
import { LoadingSpin } from "../../../utils/LoadingSpin";
import ProcessFaceRecognition from "./ProcessFaceRecognition"; 

const { Option } = Select;
const { Title } = Typography;
export default (props) => {
  const { user } = useContext(AuthContext);
  const { threshold, setFaceThresholdDistance } = useContext(
    FaceThresholdDistanceContext
  );
  const [mode, setMode] = useState(DEFAULT_ATTENDANCE_MODE);
  const [isOn, setIsOn] = useState(true);

  const [participants, setParticipants] = useState([]);
  const [facePhotos, setFacePhotos] = useState([]);
  const [faceMatcher, setFaceMatcher] = useState(null);

  const [absentees, setAbsentees] = useState([]);
  const [event, setEvent] = useState({});

  const { data, loading, error } = useQuery(
    FETCH_FACE_MATCHER_IN_COURSE_QUERY,
    {
      onError(err) {
        console.log(err);
        // props.history.push("/dashboard");
        CheckError(err);
      },
      variables: {
        eventID: props.match.params.eventID,
      },
    }
  );

  useEffect(() => {
    if (data) {
      setEvent(data.getFaceMatcherInEvent.event);
      setParticipants(data.getFaceMatcherInEvent.matcher);
      setAbsentees(data.getFaceMatcherInEvent.matcher);
      data.getFaceMatcherInEvent.matcher.map((item) => {
        item.facePhotos.map((photo) =>
          setFacePhotos((prevState) => [...prevState, photo])
        );
      });

      if (data.getFaceMatcherInEvent.matcher.length === 0) {
        message.info("Event do not have any participant yet!");
      }
    }
  }, [data, participants]);
  
  const attendanceGQLQuery = useQuery(FETCH_ATTENDANCE_QUERY, {
    onError(err) {
      props.history.push(
        `/event/${props.match.params.eventID}/attendanceList`
      );
      CheckError(err);
    },
    pollInterval: 2000,

    variables: {
      attendanceID: props.match.params.attendanceID,
    },
  });

  useEffect(() => {
    if (attendanceGQLQuery.data) {
      setMode(attendanceGQLQuery.data.getAttendance.mode);
      message.info(
        "Attendance Mode: " + attendanceGQLQuery.data.getAttendance.mode
      );
      setIsOn(attendanceGQLQuery.data.getAttendance.isOn);
      if (attendanceGQLQuery.data.getAttendance.isOn)
        message.info("Attendance is currently opened");
      else {
        if (user.userLevel == 0)
          message.info("Attendance had been closed by the host.");
        else {
          message.info(
            "You closed the attendance, no transaction will be recorded"
          );
        }
      }
    }
  }, [attendanceGQLQuery.data]);

  useEffect(() => {
    async function matcher() {
      //check there should be at least one matcher
      if (
        data.getFaceMatcherInEvent.matcher.length > 0 &&
        facePhotos.length > 0
      ) {
        const validMatcher = data.getFaceMatcherInEvent.matcher.filter(
          (m) => m.facePhotos.length > 0
        );
        const profileList = await createMatcher(validMatcher, threshold);
        setFaceMatcher(profileList);
      }
    }
    if (!!data) {
      matcher();
    }
  }, [data, facePhotos, threshold]);
 
  const { Content } = Layout;

  return (
    <Layout className="layout">
      <Navbar />
      <Layout>
        <Greeting /> 
        <Content> 
 
          {/* For F2F, use Servant PC For FR */}
          {attendanceGQLQuery.data &&
            isOn &&
            mode == "F2F" &&
            user.userLevel == 1 && (
              <ProcessFaceRecognition
                {...props}
                faceMatcher={faceMatcher}
                facePhotos={facePhotos}
                participants={participants}
              />
            )}
          {/* For Remote, use Member PC For FR */}

          {attendanceGQLQuery.data &&
            isOn &&
            mode == "Remote" &&
            user.userLevel == 0 && (
              <ProcessFaceRecognition
                {...props}
                faceMatcher={faceMatcher}
                facePhotos={facePhotos}
                participants={participants}
              />
            )}

          {!isOn && user.userLevel == 0 && (
            <Card>
              <p>The host has closed the attendance</p>
            </Card>
          )}

          <LoadingSpin loading={attendanceGQLQuery.loading} /> 
        </Content>

        <Footer />
      </Layout>
    </Layout>
  );
};
