import EventDetails from './eventDetails/EventDetails';
import Dashboard from './dashboard/Dashboard';
import { MainMenu, SignIn, SignUp } from './mainMenu';
import NoFound from './noFound/NoFound';
import Notifications from './notification/Notifications';
import Profile from './profile/Profile';
import InEventAttendanceHistory from './attendance/inEventHistory';
import SingleAttendanceHistory from './attendance/singleHistory';
import AttendanceRoom from './attendanceRoom';
import UndefinedCardIDAndRole from './undefinedCardIDAndRole';
import PrivacyPolicy from './privacyPolicy/PrivacyPolicy';
import TermCondition from './termCondition/TermCondition';
import UserGuidelines from './userGuidelines/UserGuidelines';
import Identify from './identify';
export {
  EventDetails,
  Dashboard,
  Profile,
  Notifications,
  MainMenu,
  SignIn,
  SignUp,
  NoFound,
  InEventAttendanceHistory,
  SingleAttendanceHistory,
  AttendanceRoom,
  UndefinedCardIDAndRole,
  PrivacyPolicy,
  TermCondition,
  UserGuidelines,
  Identify
};
